package bt.printer.printer;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import java.io.InputStream;
import java.util.Locale;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

import bt.printer.printer.device.BluetoothPrinter;
import bt.printer.printer.device.PrinterCommands;

/**
 * @author Konstantin on 31.05.19.
 *
 * Add next permissions:
 * <uses-permission android:name="android.permission.BLUETOOTH" />
 * <uses-permission android:name="android.permission.BLUETOOTH_ADMIN" />
 *
 * Simple printable pattern
 * -----------------------------\n
 * Address:     %address%\n
 * Distance:    %distance%\n
 * Waiting:     %wait%\n
 * Bonuses:     %bonus%\n
 * \n
 * Price:       %price%\n
 * -----------------------------\n
 *
 * Printable pattern with printer commands.
 * used commands:
 *      %nn% Set text default
 *      %nb% Set text to normal bold
 *      %mb% Set text to medium bold
 *      %lb% Set text to large bold
 *      %la% Set text align left
 *      %ca% Set text align center
 *      %ra% Set text align right
 *
 * %ca%%mb%Header Text\n
 * %ca%-----------------------------\n
 * %nb%Order №:       %mb%%id_order%\n
 * Address:       %address%\n
 * Distance:      %distance%\n
 * Waiting:       %wait%\n
 * Bonuses:       %bonus%\n
 * \n
 * %lb%Total price:     %nn%%price% usd\n
 * %ca%-----------------------------\n
 * %ca%Final message with congratulations of print success
 */

public class PrinterHelper extends ContextWrapper {
    private PrinterParameter mParameter;
    private BluetoothPrinter mPrinter = null;
    public PrinterHelper(Context base) {
        super(base);
        mParameter = new PrinterParameter(PreferenceManager.getDefaultSharedPreferences(this));
        if(mParameter.isAvailable) {
            try {
                mPrinter = new BluetoothPrinter(mParameter.mac);
            }catch (Exception ex){
                mParameter = null;
            }
        }
    }

    public void getPrintOrder(final String pattern, @Nullable final String id, final String item, final int weight, final int delivery, final double price){
        if(mPrinter!=null && pattern!=null && !pattern.isEmpty()) {
            mPrinter.connectPrinter(new BluetoothPrinter.PrinterConnectListener() {
                @Override
                public void onConnected(BluetoothPrinter.Commands cmd, InputStream stream) {
                    cmd.printUnicode(PrinterCommands.INIT);

                    for (String line : pattern.split("\n")){
                        cmd.printUnicode(PrinterCommands.RESET_PRINTER);
                        cmd.printUnicode(PrinterCommands.ESC_ALIGN_LEFT);

                        byte[] char_table = !mParameter.isOnTranscription ?
                                PrinterCommands.SELECT_CYRILLIC_CHARACTER_CODE_TABLE : PrinterCommands.SELECT_MULTILINGUAL_CHARACTER_CODE_TABLE;
                        cmd.printUnicode(char_table);


                        //подмена тегов
                        if(line.contains("%order%")) line = line.replace("%order%", id!=null ? id : "0");
                        if(line.contains("%item%")) line = line.replace("%item%", item);
                        if(line.contains("%weight%")) line = line.replace("%weight%", String.valueOf(weight));
                        if(line.contains("%delivery%")) line = line.replace("%delivery%", getFormatTime(delivery));
                        if(line.contains("%price%")) line = line.replace("%price%", String.valueOf(price));

                        Command[] tags = new Command[]{
                                new Command("%nn%" , PrinterCommands.TEXT_DEFAULT),
                                new Command("%nb%" , PrinterCommands.TEXT_NORMAL_BOLD),
                                new Command("%mb%" , PrinterCommands.TEXT_MEDIUM_BOLD),
                                new Command("%lb%" , PrinterCommands.TEXT_LARGE_BOLD),
                                new Command("%la%" , PrinterCommands.ESC_ALIGN_LEFT),
                                new Command("%ca%" , PrinterCommands.ESC_ALIGN_CENTER),
                                new Command("%ra%" , PrinterCommands.ESC_ALIGN_RIGHT),
                                new Command("%dl%" , PrinterCommands.FEED_PAPER_AND_CUT)
                        };

                        //собираем команды в сортированный массив исключая дубли начиная с определенного индекса строки
                        TreeMap<Integer, byte[]> comm = new TreeMap<>();
                        for(int i=0; i<=line.length(); i++){
                            for(Command tag : tags) {
                                int index = line.indexOf(tag.getCommand(), i);
                                if (index >= 0) comm.put(index, tag.getVal());
                            }
                        }

                        if(comm.size() > 0) {
                            int know = comm.firstKey();
                            //если в строке первое это печатный текст, а не команда
                            if(know>0 && know+4 < line.length()){
                                //Log.e("TEST", "Print String: " + line.substring(0, comm.first()));
                                String input = line.substring(0, comm.firstKey());
                                cmd.addPrintBuffer(input, mParameter.isOnTranscription);
                            }

                            //выберем команды и их печатный текст
                            //команды вбираются до печатного к которому применены
                            for(Integer ind : comm.keySet()) {
                                if(ind > know){
                                    //Log.e("TEST", "ind:" + ind + " Print String: " + line.substring(know, ind));
                                    String input = line.substring(know, ind);
                                    cmd.addPrintBuffer(input, mParameter.isOnTranscription);
                                    cmd.addPrintBuffer(PrinterCommands.TEXT_DEFAULT);
                                }
                                cmd.addPrintBuffer(comm.get(ind));
                                know = ind+4;
                            }

                            //если команды закончились, а текст для печати остался
                            if(comm.lastKey()+4 < line.length()){
                                String input = line.substring(comm.lastKey()+4);
                                cmd.addPrintBuffer(input, mParameter.isOnTranscription);
                                cmd.addPrintBuffer(PrinterCommands.TEXT_DEFAULT);
                            }
                        }else cmd.addPrintBuffer(line, mParameter.isOnTranscription);
                        cmd.bufferPrint();
                        cmd.printUnicode(PrinterCommands.TEXT_DEFAULT);
                        cmd.printUnicode(PrinterCommands.LF);
                        cmd.clearBuffer();
                    }

                    cmd.addNewLine(2);
                    mPrinter.finish();
                }

                @Override
                public void onFailed(Exception e) {
                    Log.e("PRINTER", e.getMessage());
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    mPrinter.finish();
                }
            });
        }
    }

    private class Command{
        private String $command;
        private byte[] $val;
        Command(String command, byte[] val){
            this.$command = command;
            this.$val = val;
        }

        String getCommand() {
            return $command;
        }

        byte[] getVal() {
            return $val;
        }
    }


    private String getFormatTime(int sec){
        return String.format(Locale.getDefault(), "%02d:%02d:%02d", TimeUnit.SECONDS.toHours(sec),
                TimeUnit.SECONDS.toMinutes(sec) - TimeUnit.HOURS.toMinutes(TimeUnit.SECONDS.toHours(sec)), // The change is in this line
                TimeUnit.SECONDS.toSeconds(sec) - TimeUnit.MINUTES.toSeconds(TimeUnit.SECONDS.toMinutes(sec)));
    }

    class PrinterParameter{
        private boolean isAvailable = false;
        private boolean isOnTranscription = false;
        private String mac = null;

        PrinterParameter(@NonNull SharedPreferences preferences){
            this.isOnTranscription = preferences.getBoolean("bluetooth_transcription", false);
            this.mac = preferences.getString("bluetooth_mac", "");
            this.isAvailable = preferences.getBoolean("bluetooth_printer", false) && this.mac!=null && !this.mac.isEmpty();
        }
    }
}
