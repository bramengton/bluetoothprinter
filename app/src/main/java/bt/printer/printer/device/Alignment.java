package bt.printer.printer.device;

/**
 * @author Konstantin on 06.08.19.
 */
public enum Alignment{
    LEFT,
    CENTER,
    RIGHT;

    public byte[] getAlignment(){
        switch (this){
            case LEFT: return PrinterCommands.ALIGNMENT_LEFT;
            case CENTER: return PrinterCommands.ALIGNMENT_CENTER;
            case RIGHT: return PrinterCommands.ALIGNMENT_RIGHT;
        }
        return PrinterCommands.ALIGNMENT_LEFT;
    }
}
