package bt.printer.printer.device;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import bt.printer.R;

/**
 * @author Konstantin on 30.05.19.
 */
public class CustomAdapter extends ArrayAdapter<BondedDevice> {

    private LayoutInflater mInflater;

    public CustomAdapter(Context context, List<BondedDevice> list){
        super(context, R.layout.spinner_item, list);
        setDropDownViewResource(android.R.layout.simple_list_item_checked);
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getDevicePosition(String mac){
        for(int i=0; i<getCount(); i++){
            BondedDevice dev = getItem(i);
            if(dev!=null && dev.getMac().equals(mac)) return i;
        }
        return 0;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return RowView(convertView,position);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return RowView(convertView,position);
    }

    private View RowView(View convertView , int position){
        viewHolder holder ;
        View rowview = convertView;
        if (rowview==null) {
            holder = new viewHolder();
            rowview = mInflater.inflate(R.layout.spinner_item, null, false);

            holder.mName = rowview.findViewById(android.R.id.text1);
            rowview.setTag(holder);
        }else{
            holder = (viewHolder) rowview.getTag();
        }

        BondedDevice rowItem = getItem(position);
        if(rowItem!=null) {
            holder.mName.setTextColor(position == 0 ? Color.GRAY : Color.BLACK);
            holder.mName.setSingleLine(true);
            if(rowItem.getMac().isEmpty()) holder.mName.setText(rowItem.getName());
            else {
                holder.mName.setSingleLine(false);
                holder.mName.setText(String.format("%s\n%s", rowItem.getName(), rowItem.getMac()));
            }
        }

        return rowview;
    }

    private class viewHolder{
        TextView mName;
    }
}