package bt.printer.printer.device;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * @author Konstantin on 30.05.19.
 */
public class BondedDevice implements Parcelable {
    private String mName = "";
    private String mMac = "";

    public BondedDevice(){
        this.mName = "";
        this.mMac = "";
    }

    BondedDevice(String name, String address){
        this.mName = name;
        this.mMac = address;
    }

    public BondedDevice(String name){
        this(name, "");
    }

    public String getName() {
        return this.mName;
    }

    public String getMac() {
        return this.mMac;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mName);
        dest.writeString(this.mMac);
    }

    protected BondedDevice(Parcel in) {
        this.mName = in.readString();
        this.mMac = in.readString();
    }

    public static final Parcelable.Creator<BondedDevice> CREATOR = new Parcelable.Creator<BondedDevice>() {
        @Override
        public BondedDevice createFromParcel(Parcel source) {
            return new BondedDevice(source);
        }

        @Override
        public BondedDevice[] newArray(int size) {
            return new BondedDevice[size];
        }
    };
}
