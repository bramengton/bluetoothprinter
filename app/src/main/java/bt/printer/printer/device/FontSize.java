package bt.printer.printer.device;

/**
 * @author Konstantin on 06.08.19.
 */
public enum FontSize {
    NORMAL,
    NORMAL_BOLD,
    MEDIUM_BOLD,
    LARGE_BOLD,
}
