package bt.printer.printer.device;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;

import com.ibm.icu.text.Transliterator;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author Konstantin on 30.05.19.
 */
public class BluetoothPrinter {

    public static final String CYRILLIC_TO_LATIN = "Any-Latin; NFD; Latin-Ascii";

    private BluetoothDevice mPrinter;
    private BluetoothSocket mSocket = null;
    private Commands mCommands;
    private BluetoothAdapter mAdapter;

    public BluetoothPrinter() {
        mAdapter = BluetoothAdapter.getDefaultAdapter();
    }

    public BluetoothPrinter(String address) throws Exception {
        this();
        changeDevice(address);
    }

    public void changeDevice(String address) throws Exception {
        if(address!=null && !address.isEmpty()) {
            this.mPrinter = mAdapter.getRemoteDevice(address);
        }else {
            //в случае если устройство не рабочее / не найдено / не отвечает / любая другая ошибка,
            // обнуляем чтоб небыло попыток подключения - значит спама новыми сообщ. об ошибках.
            mPrinter = null;
            throw new Exception("Device address empty/unknown");
        }
    }

    public void connectPrinter(@NonNull final PrinterConnectListener listener) {
        if(mPrinter !=null) {
            new ConnectTask(new ConnectTask.BtConnectListener() {
                @Override
                public void onConnected(BluetoothSocket socket) {
                    mSocket = socket;
                    try {
                        mCommands = new Commands(socket.getOutputStream());
                        listener.onConnected(mCommands, socket.getInputStream());
                    } catch (IOException e) {
                        listener.onFailed(e);
                        finish();
                    }
                }

                @Override
                public void onFailed() {
                    listener.onFailed(new Exception("Can't init Bluetooth socket, device unavailable"));
                }
            }).execute(mPrinter);
        }
    }

    public boolean isConnected() {
        return mSocket != null && mSocket.isConnected();
    }

    public void finish() {
        if (mSocket != null) {
            try {
                mCommands.btOutputStream.close();
                mCommands.clearBuffer();
                mSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            mSocket = null;
        }
    }

    private static class ConnectTask extends AsyncTask<BluetoothDevice, Void, BluetoothSocket> {
        private BtConnectListener listener;
        private String METHOD = "createRfcommSocket";

        private ConnectTask(BtConnectListener listener) {
            this.listener = listener;
        }

        @Override
        protected BluetoothSocket doInBackground(BluetoothDevice... bluetoothDevices) {
            BluetoothDevice device = bluetoothDevices[0];
            //if(device.getUuids()==null || device.getUuids().length>0) return null;
            UUID uuid = device.getUuids()[0].getUuid();
            BluetoothSocket socket = null;
            boolean connected = true;
            try {
                socket = device.createRfcommSocketToServiceRecord(uuid);
                socket.connect();
            } catch (IOException e) {
                try {
                    socket = (BluetoothSocket) device.getClass()
                            .getMethod(METHOD, new Class[]{int.class})
                            .invoke(device, 1);
                    socket.connect();
                } catch (Exception e2) {
                    connected = false;
                }
            }
            return connected ? socket : null;
        }

        @Override
        protected void onPostExecute(BluetoothSocket bluetoothSocket) {
            if (listener != null) {
                if (bluetoothSocket != null) listener.onConnected(bluetoothSocket);
                else listener.onFailed();
            }
        }

        private interface BtConnectListener {
            void onConnected(BluetoothSocket socket);
            void onFailed();
        }
    }

    public interface PrinterConnectListener {
        void onConnected(Commands cmd, InputStream stream);
        void onFailed(Exception ex);
    }

    public BluetoothSocket getSocket() {
        return mSocket;
    }

    public BluetoothDevice getDevice() {
        return mPrinter;
    }

    public List<BondedDevice> getDevices(String default_name){
        List<BondedDevice> list =  new ArrayList<>();
        list.add(new BondedDevice(default_name));
        for(BluetoothDevice val :mAdapter.getBondedDevices()){
            list.add(new BondedDevice(val.getName(), val.getAddress()));
        }
        return list;
    }

    public static class Commands{
        private OutputStream btOutputStream = null;
        private List<byte[]> btBuffer = new ArrayList<>();


        Commands(OutputStream stream){
            this.btOutputStream = stream;
            btBuffer = new ArrayList<>();
        }


        public boolean printString(String text, boolean transcript){
            return printString(text, FontSize.NORMAL, Alignment.LEFT, transcript);
        }

        public boolean printString(String text, FontSize fontSize,  boolean transcript){
            return printString(text, fontSize, Alignment.LEFT, transcript);
        }

        public boolean printString(String text, FontSize fontSize, Alignment alignment,  boolean transcript){
            try {
                printCustom(
                        transcript ? getTranscriptor(text) : text,
                        fontSize,
                        alignment,
                        transcript ? "UTF8" : "CP866"
                ); //"UTF8" : "CP866"
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }


        public void addPrintBuffer(byte[] input){
            btBuffer.add(input);
        }


        public void addPrintBuffer(String input, boolean transcript){
            try {
                byte[] text = transcript ?
                        getTranscriptor(input).getBytes() :
                        input.getBytes(transcript ? "UTF8" : "CP866");
                addPrintBuffer(text);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }

        public void bufferPrint(){
            for (byte[] buffer : btBuffer){
                printUnicode(buffer);
            }
        }

        public void clearBuffer(){
            btBuffer = new ArrayList<>();
        }

        public boolean printText(String msg) {
            return printUnicode(msg.getBytes());
        }

        public boolean printUnicode(byte msg) {
            try {
                this.btOutputStream.write(msg);
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }

        public boolean printUnicode(byte[] data) {
            try {
                this.btOutputStream.write(data);
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }

        public boolean printUnicode() {
            try {
                this.btOutputStream.write(Utils.UNICODE_TEXT);
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }

        //print custom
        private void printCustom(String msg, FontSize size, Alignment align, String charsetName) throws IOException {
            switch (size) {
                case NORMAL:
                    this.btOutputStream.write(PrinterCommands.TEXT_NORMAL);
                    break;
                case NORMAL_BOLD:
                    this.btOutputStream.write(PrinterCommands.TEXT_NORMAL_BOLD);
                    break;
                case MEDIUM_BOLD:
                    this.btOutputStream.write(PrinterCommands.TEXT_MEDIUM_BOLD);
                    break;
                case LARGE_BOLD:
                    this.btOutputStream.write(PrinterCommands.TEXT_LARGE_BOLD);
                    break;
            }

            switch (align) {
                case LEFT: //left align
                    this.btOutputStream.write(PrinterCommands.ESC_ALIGN_LEFT);
                    break;
                case CENTER: //center align
                    this.btOutputStream.write(PrinterCommands.ESC_ALIGN_CENTER);
                    break;
                case RIGHT: //right align
                    this.btOutputStream.write(PrinterCommands.ESC_ALIGN_RIGHT);
                    break;
            }
            this.btOutputStream.write(charsetName!=null && !charsetName.isEmpty() ? msg.getBytes(charsetName) : msg.getBytes());
            this.btOutputStream.write(PrinterCommands.LF);
            this.btOutputStream.write(PrinterCommands.TEXT_DEFAULT);
        }

        public boolean addNewLine() {
            return addNewLine(1)>0;
        }

        public int addNewLine(int count) {
            int success = 0;
            for (int i = 0; i < count; i++) {
                if (printUnicode(PrinterCommands.LINE_FEED)) success++;
            }
            return success;
        }

        public boolean printPhotoBW(Bitmap var1) {
            Bitmap var4 = var1.copy(Bitmap.Config.ARGB_8888, true);
            if (var4 == null) {
                return false;
            } else {
                printUnicode(Utils.BinarizeBWImage(var4));
                return true;
            }
        }

        //print photo
        public void printPhoto(Bitmap bmp) {
            try {
//                Bitmap bmp = BitmapFactory.decodeResource(getResources(), img);
                if(bmp!=null){
                    Bitmap var4 = bmp.copy(Bitmap.Config.ARGB_8888, true);
                    if (var4 != null) printUnicode(Utils.decodeBitmap(bmp));
                }else{
                    Log.e("Print Photo error", "the file isn't exists");
                }
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("PrintTools", "the file isn't exists");
            }
        }


        public boolean setFontStyle(boolean var1, boolean var2, FontSize font, FontType type) {
            try{
                byte[] size = PrinterCommands.FONT_NORMAL;
                byte var6 = 0;
                if (var1) var6 = 8;
                if (var2) var6 = (byte)(var6 + 128);
                switch(font) {
                    case NORMAL_BOLD:
                        var6 = (byte)(var6 + 16);
                        break;
                    case MEDIUM_BOLD:
                        var6 = (byte)(var6 + 32);
                        break;
                    case LARGE_BOLD:
                        var6 = (byte)(var6 + 48);
                }

                switch(type) {
                    case FONT_A:
                        var6 = (byte)(var6 + 0);
                        break;
                    case FONT_B:
                        ++var6;
                }
                size[2] = var6;
                this.btOutputStream.write(size);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return true;
        }

        public void getStatuses(){
            printUnicode(PrinterCommands.BATTERY_STATUS);
            printUnicode(PrinterCommands.PLATTEN_STATUS);
            printUnicode(PrinterCommands.PAPER_STATUS);
            printUnicode(PrinterCommands.VERSION_NUMBER);
            printUnicode(PrinterCommands.TEMP_STATUS);
            printUnicode(PrinterCommands.PRINTER_BUFFER_STATUS);
        }

        public void setCenter(){
            printUnicode(PrinterCommands.ESC_HORIZONTAL_CENTERS);
        }

        public void setAlignment(Alignment alignment){
            printUnicode(alignment.getAlignment());
        }

        public void setCharRightSpacing(int var1) {
            var1 = var1<0 ? 0 : (var1>255 ? 255 : var1);
            printUnicode(new byte[]{27, 32, (byte)var1});
        }

        public void pixelLineFeed(int var1) {
            var1 = var1<0 ? 0 : (var1>255 ? 255 : var1);
            printUnicode(new byte[]{27, 74, (byte)var1});
        }



        public void printLineFeed() {
            printUnicode(PrinterCommands.LINE_FEED);
        }

        public void setLineSpacing(int lineSpacing) {
            byte[] cmd = PrinterCommands.SET_LINE_SPACING;
            cmd[2] = (byte) lineSpacing;
            printUnicode(cmd);
        }

        public void setBold(boolean bold) {
            printUnicode(bold ? PrinterCommands.BOLD_ON : PrinterCommands.BOLD_OFF);
        }

        public void feedPaper(int lines) {
            for (int i=0; i<lines; i++) {
                addNewLine();
            }
        }


        private String getTranscriptor(String in) {
            return Transliterator.getInstance(CYRILLIC_TO_LATIN).transliterate(in);
        }

        public byte[] convertExtendedAscii(String input){
            int length = input.length();
            byte[] retVal = new byte[length];
            for(int i=0; i<length; i++) {
                char c = input.charAt(i);
                retVal[i] = c < 127 ? (byte)c : (byte)(c - 256);
            }
            return retVal;
        }

        private byte[] decodeText(String text, String encoding) throws CharacterCodingException, UnsupportedEncodingException {
            Charset charset = Charset.forName(encoding);
            CharsetDecoder decoder = charset.newDecoder();
            CharsetEncoder encoder = charset.newEncoder();
            ByteBuffer bbuf = encoder.encode(CharBuffer.wrap(text));
            CharBuffer cbuf = decoder.decode(bbuf);
            String s = cbuf.toString();
            return s.getBytes(encoding);
        }

        public enum FontType {
            FONT_A,
            FONT_B
        }

        public void finish(){
            try {
                this.btOutputStream.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
