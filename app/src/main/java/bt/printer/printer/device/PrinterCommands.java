package bt.printer.printer.device;

/**
 * @author Konstantin on 30.05.19.
 */
public final class PrinterCommands  {
    public static final byte HT = 0x9;
    public static final byte LF = 0x0A;
    public static final byte CR = 0x0D;
    public static final byte ESC = 0x1B;
    public static final byte DLE = 0x10;
    public static final byte GS = 0x1D;
    public static final byte FS = 0x1C;
    public static final byte STX = 0x02;
    public static final byte US = 0x1F;
    public static final byte CAN = 0x18;
    public static final byte CLR = 0x0C;
    public static final byte EOT = 0x04;

//    public static final byte[] INIT = {27, 64};
    public static final byte[] INIT = {0x1B,0x21,0x03};
    public static byte[] FEED_LINE = {10};

    public static byte[] SELECT_FONT_A = {20, 33, 0};

    public static byte[] SET_BAR_CODE_HEIGHT = {29, 104, 100};
    public static byte[] PRINT_BAR_CODE_1 = {29, 107, 2};
    public static byte[] SEND_NULL_BYTE = {0x00};

    public static byte[] SELECT_PRINT_SHEET = {0x1B, 0x63, 0x30, 0x02};
    public static byte[] FEED_PAPER_AND_CUT = {0x1D, 0x56, 66, 0x00};

    public static byte[] SELECT_CYRILLIC_CHARACTER_CODE_TABLE = {0x1B, 0x74, 0x11};
    public static byte[] SELECT_MULTILINGUAL_CHARACTER_CODE_TABLE = {0x1B, 0x74, 0x02}; // 2 - OEM850
//    public static byte[] SELECT_MULTILINGUAL_CHARACTER_CODE_TABLE = {0x1B, 0x74, 0x35}; // 53 - OEM858
    //27, 116, 2

    public static byte[] SELECT_BIT_IMAGE_MODE = {0x1B, 0x2A, 33, -128, 0};
//        public static byte[] SELECT_BIT_IMAGE_MODE = {0x1B, 0x2A, 33, 0xff, 3};
    public static byte[] SET_LINE_SPACING = {27, 51, 0};
    public static byte[] SET_LINE_SPACING_24 = {0x1B, 0x33, 24};
    public static byte[] SET_LINE_SPACING_30 = {0x1B, 0x33, 30};

    public static byte[] TRANSMIT_DLE_PRINTER_STATUS = {0x10, 0x04, 0x01};
    public static byte[] TRANSMIT_DLE_OFFLINE_PRINTER_STATUS = {0x10, 0x04, 0x02};
    public static byte[] TRANSMIT_DLE_ERROR_STATUS = {0x10, 0x04, 0x03};
    public static byte[] TRANSMIT_DLE_ROLL_PAPER_SENSOR_STATUS = {0x10, 0x04, 0x04};

    public static final byte[] ESC_FONT_COLOR_DEFAULT = new byte[] { 0x1B, 'r',0x00 };
    public static final byte[] FS_FONT_ALIGN = new byte[] { 0x1C, 0x21, 1, 0x1B, 0x21, 1 };
    public static final byte[] ESC_ALIGN_LEFT = new byte[] { 0x1b, 'a', 0x00 };
    public static final byte[] ESC_ALIGN_RIGHT = new byte[] { 0x1b, 'a', 0x02 };
    public static final byte[] ESC_ALIGN_CENTER = new byte[] { 0x1b, 'a', 0x01 };
    public static final byte[] ESC_CANCEL_BOLD = new byte[] { 0x1B, 0x45, 0 };


    /*********************************************/
    public static final byte[] ESC_HORIZONTAL_CENTERS = new byte[] { 0x1B, 0x44, 20, 28, 00};
    public static final byte[] ESC_CANCLE_HORIZONTAL_CENTERS = new byte[] { 0x1B, 0x44, 00 };
    /*********************************************/

    public static final byte[] ESC_ENTER = new byte[] { 0x1B, 0x4A, 0x40 };
    public static final byte[] PRINTE_TEST = new byte[] { 0x1D, 0x28, 0x41 };





    ///***********************************************************************




    public static final byte[] RESET_PRINTER = new byte[]{27, 64};
    public static final byte[] PRINTER_TEST = new byte[]{29, 65};
    public static final byte[] ALIGNMENT_LEFT = new byte[]{27, 97, 0};
    public static final byte[] ALIGNMENT_CENTER = new byte[]{27, 97, 1};
    public static final byte[] ALIGNMENT_RIGHT = new byte[]{27, 97, 2};

    public static final byte[] TEXT_DEFAULT = new byte[]{0x1B,0x21,0x00};
    public static final byte[] TEXT_NORMAL = new byte[]{27,33,3};  // 0- normal size text
    //byte[] cc1 = new byte[]{27,33,0};  // 0- normal size text
    public static final byte[] TEXT_NORMAL_BOLD = new byte[]{27,33,8};  // 1- only bold text
    public static final byte[] TEXT_MEDIUM_BOLD = new byte[]{27,33,32}; // 2- bold with medium text
    public static final byte[] TEXT_LARGE_BOLD = new byte[]{27,33,16}; // 3- bold with large text
    public static final byte[] TEXT_DOUBLE_WH = new byte[]{27, 33, 48};


    public static final byte[] BOLD_ON = new byte[]{27, 69, 1};
    public static final byte[] BOLD_OFF = new byte[]{27, 69, 0};

    public static final byte[] TAB = new byte[]{9};
    public static final byte[] BLACK_MARK_FEED = new byte[]{29, 40, 69, 0, 0, 16};
    public static final byte[] LINE_FEED = new byte[]{10};
    public static final byte[] REVERSE_LINE_FEED = new byte[]{27, 101, 1};
    public static final byte[] STYLE_FIXEDSYS = new byte[]{27, 48};
    public static final byte[] STYLE_COURIER = new byte[]{27, 49};
    public static final byte[] STYLE_HINDI = new byte[]{27, 50};

    public static final byte[] FONT_SIZE_LOW = new byte[]{29, 68, 0};
    public static final byte[] FONT_SIZE_NORMAL = new byte[]{29, 68, 1};
    public static final byte[] FONT_SIZE_HIGH = new byte[]{29, 68, 3};

    public static final byte[] FONT_NORMAL = new byte[]{27, 33, 0};


    public static byte[] BATTERY_STATUS = new byte[]{27, 121};
    public static byte[] PLATTEN_STATUS = new byte[]{27, 112};
    public static byte[] PAPER_STATUS = new byte[]{27, 80};
    public static byte[] VERSION_NUMBER = new byte[]{27, 86};
    public static byte[] TEMP_STATUS = new byte[]{27, 104};
    public static byte[] PRINTER_BUFFER_STATUS = new byte[]{29, 114};
    public static int[] p0 = new int[]{0, 128};
    public static int[] p1 = new int[]{0, 64};
    public static int[] p2 = new int[]{0, 32};
    public static int[] p3 = new int[]{0, 16};
    public static int[] p4 = new int[]{0, 8};
    public static int[] p5 = new int[]{0, 4};
    public static int[] p6 = new int[]{0, 2};

    //======================================================================================

//    public static final byte HT = 9;
//    public static final byte LF = 10;
//    public static final byte CR = 13;
//    public static final byte ESC = 27;
//    public static final byte DLE = 16;
//    public static final byte GS = 29;
//    public static final byte FS = 28;
//    public static final byte STX = 2;
//    public static final byte US = 31;
//    public static final byte CAN = 24;
//    public static final byte CLR = 12;
//    public static final byte EOT = 4;
//
//
//
//    public static byte[] SELECT_FONT_A = {20, 33, 24};
//
//    public static byte[] SET_BAR_CODE_HEIGHT = {29, 104, 100};
//    public static byte[] PRINT_BAR_CODE_1 = {29, 107, 2};
//    public static byte[] SEND_NULL_BYTE = {0};
//
//    public static byte[] SELECT_PRINT_SHEET = {27, 99, 48, 2};
//    public static byte[] FEED_PAPER_AND_CUT = {29, 86, 66, 0};
//
////    public static byte[] SELECT_CYRILLIC_CHARACTER_CODE_TABLE = {27, 116, 17};
////    public static byte[] SELECT_CYRILLIC_CHARACTER_CODE_TABLE = {27, 116, 19};
//    public static byte[] SELECT_CYRILLIC_CHARACTER_CODE_TABLE = {0x1B, 0x74, 0x11};
//
//
//    public static byte[] SELECT_BIT_IMAGE_MODE = {27, 42, 33, -128, 0};
//    public static byte[] SET_LINE_SPACING = {27, 51, 0};
//    public static byte[] SET_LINE_SPACING_24 = {27, 51, 24};
//    public static byte[] SET_LINE_SPACING_30 = {27, 51, 30};
//
//    public static byte[] TRANSMIT_DLE_PRINTER_STATUS = {16, 4, 1};
//    public static byte[] TRANSMIT_DLE_OFFLINE_PRINTER_STATUS = {16, 4, 2};
//    public static byte[] TRANSMIT_DLE_ERROR_STATUS = {16, 4, 3};
//    public static byte[] TRANSMIT_DLE_ROLL_PAPER_SENSOR_STATUS = {16, 4, 4};
//
//    public static final byte[] ESC_FONT_COLOR_DEFAULT = new byte[] { 27, 'r', 0 };
//    public static final byte[] FS_FONT_ALIGN = new byte[] { 28, 33, 1, 27, 33, 1 };
//    public static final byte[] ESC_ALIGN_LEFT = new byte[] { 27, 'a', 0 };
//    public static final byte[] ESC_ALIGN_RIGHT = new byte[] { 27, 'a', 2 };
//    public static final byte[] ESC_ALIGN_CENTER = new byte[] { 27, 'a', 1 };
//    public static final byte[] ESC_CANCEL_BOLD = new byte[] { 27, 69, 0 };
//
//
//    /*********************************************/
//    public static final byte[] ESC_HORIZONTAL_CENTERS = new byte[] { 27, 68, 20, 28, 0};
//    public static final byte[] ESC_CANCLE_HORIZONTAL_CENTERS = new byte[] { 27, 68, 0 };
//    /*********************************************/
//
//    public static final byte[] ESC_ENTER = new byte[] { 27, 74, 64 };

}
