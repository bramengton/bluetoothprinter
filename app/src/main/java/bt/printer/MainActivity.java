package bt.printer;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import bt.printer.printer.PrinterHelper;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        findViewById(R.id.settings).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(v.getContext(), PrinterSettingsDialog.class), PrinterSettingsDialog.REQUEST_CODE_OK);
            }
        });

        findViewById(R.id.print).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String id_order = "123456";
                String item = "Great item";
                int weight = 100;
                int delivery = 456300;
                double price = 777;

                String pattern = "%ca%%mb%Торговая компания\n" +
                        "%ca%-----------------------------\n" +
                        "%nb%Заказ №:       %mb%%order%\n" +
                        "Title:             %item%\n" +
                        "Weight:            %weight%kg\n" +
                        "Delivery:          %delivery%\n" +
                        "\n" +
                        "%lb%Total price:     %nn%%price% usd\n" +
                        "%ca%-----------------------------\n" +
                        "%ca%Final message with congratulations of print success.";

                new PrinterHelper(v.getContext())
                        .getPrintOrder(
                                pattern,
                                id_order,
                                item,
                                weight,
                                delivery,
                                price);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && data != null) {
            Bundle bundle = data.getExtras();
            if (requestCode == PrinterSettingsDialog.REQUEST_CODE_OK) {
                if (bundle != null && bundle.containsKey(PrinterSettingsDialog.BUNDLE_RESULT)) {}
            }
        }
    }
}
