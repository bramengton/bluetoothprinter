package bt.printer;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.InputStream;

import bt.printer.printer.device.Alignment;
import bt.printer.printer.device.BluetoothPrinter;
import bt.printer.printer.device.BondedDevice;
import bt.printer.printer.device.CustomAdapter;
import bt.printer.printer.device.FontSize;
import bt.printer.printer.device.PrinterCommands;

/**
 * @author Konstantin on 30.05.19.
 */
public class PrinterSettingsDialog extends ActivityDialog {
    public static final String BUNDLE_RESULT = "SelectPrinterActivityResult";
    public static final int REQUEST_CODE_OK = 999;

    private BondedDevice device;
    private boolean isTranscriptionReq = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_bluetooth_printer);
        final TextView textView = findViewById(R.id.name);
        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);

        final CheckBox useBltPrinter = findViewById(R.id.checkBox);
        isTranscriptionReq = preferences.getBoolean("bluetooth_transcription", false);
        useBltPrinter.setChecked(isTranscriptionReq);
        useBltPrinter.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isTranscriptionReq = isChecked;
                preferences.edit().putBoolean("bluetooth_transcription", isChecked).apply();
            }
        });

        final Button test = findViewById(R.id.test);
        final BluetoothPrinter mPrinter = new BluetoothPrinter();
        Spinner spinner = findViewById(R.id.devices);

        CustomAdapter adapter = new CustomAdapter(this, mPrinter.getDevices(getString(R.string.select_device)));
        spinner.setAdapter(adapter);
        int pos = adapter.getDevicePosition(preferences.getString("bluetooth_mac", ""));
        spinner.setSelection(pos);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long id) {
                if(position>0) {
                    device = ((BondedDevice) arg0.getAdapter().getItem(position));
                    textView.setText(device.getName());
                    test.setEnabled(true);
                }else{
                    textView.setText(android.R.string.unknownName);
                    useBltPrinter.setChecked(false);
                    test.setEnabled(false);
                    device = new BondedDevice();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {}
        });



        test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    mPrinter.changeDevice(device.getMac());
                    mPrinter.connectPrinter(new BluetoothPrinter.PrinterConnectListener() {
                        @Override
                        public void onConnected(BluetoothPrinter.Commands cmd, InputStream stream) {
                            cmd.printUnicode(PrinterCommands.RESET_PRINTER);
                            cmd.printUnicode(PrinterCommands.TEXT_DEFAULT);
                            if(!isTranscriptionReq) cmd.printUnicode(PrinterCommands.SELECT_CYRILLIC_CHARACTER_CODE_TABLE);
                            else cmd.printUnicode(PrinterCommands.SELECT_MULTILINGUAL_CHARACTER_CODE_TABLE);

                            cmd.printText("-----------------------------\n");
                            cmd.printString("Print TEST", FontSize.MEDIUM_BOLD, Alignment.CENTER, isTranscriptionReq);
                            cmd.printUnicode(PrinterCommands.ESC_ALIGN_LEFT);
                            cmd.printText("-----------------------------\n");
                            cmd.printString("En: Test string to print", isTranscriptionReq);
                            //Украинский поддерживается частично и требует замены символов на аналогичные из латиницы
                            cmd.printString("Uk: Тестовий рядок для друку", isTranscriptionReq);
                            cmd.printString("Ru: Тестовая строка для печати", isTranscriptionReq);
                            cmd.printText("-----------------------------\n");
                            cmd.printUnicode(PrinterCommands.TEXT_DEFAULT);
                            cmd.addNewLine(2);
                            cmd.finish();
                            mPrinter.finish();
                        }

                        @Override
                        public void onFailed(Exception e) {
                            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    });
                }catch (Exception e){
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });

        findViewById(R.id.btn_yes).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean result = !device.getMac().isEmpty();
                preferences.edit().putString("bluetooth_mac", device.getMac()).apply();
                preferences.edit().putBoolean("bluetooth_printer", result).apply();
                setResult(RESULT_OK, new Intent().putExtra(PrinterSettingsDialog.BUNDLE_RESULT, result));
                finish();
            }
        });

        findViewById(R.id.btn_no).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean result = false;
                preferences.edit().putBoolean("bluetooth_printer", result).apply();
                setResult(RESULT_OK, new Intent().putExtra(PrinterSettingsDialog.BUNDLE_RESULT, result));
                finish();
            }
        });
    }
}
