package bt.printer;

import android.app.Activity;
import android.util.DisplayMetrics;
import android.view.Window;
import android.view.WindowManager;

/**
 * @author Konstantin on 02.05.19.
 */
public abstract class ActivityDialog extends Activity {

    /**
     * Расчет минимальной ширины диалога от размера экрана
     * @return int windowMinWidthMajor
     */
    private int windowMinWidthMajor(int persent) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
//        int width = Resources.getSystem().getDisplayMetrics().widthPixels;
        return (width*persent/100);
    }

    @Override
    public void setContentView(int layoutResID) {
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        super.setContentView(layoutResID);

        int minWidthPercents = getResources().getInteger(R.integer.minWidthMajor);

        window.setLayout(windowMinWidthMajor(minWidthPercents), WindowManager.LayoutParams.WRAP_CONTENT);
    }
}
